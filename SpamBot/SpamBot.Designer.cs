﻿namespace SpamBot {
	partial class SpamBot {
		private System.ComponentModel.IContainer components = null;

		protected override void Dispose(bool disposing) {
			if (disposing && (components != null))
				components.Dispose();

			base.Dispose(disposing);
		}

		#region Kod generowany przez Projektanta formularzy systemu Windows

		private void InitializeComponent()
		{
			this.components = new System.ComponentModel.Container();
			this.TextBox1 = new System.Windows.Forms.TextBox();
			this.Button2 = new System.Windows.Forms.Button();
			this.Label1 = new System.Windows.Forms.Label();
			this.Timer1 = new System.Windows.Forms.Timer(this.components);
			this.Button1 = new System.Windows.Forms.Button();
			this.SuspendLayout();
			// 
			// TextBox1
			// 
			this.TextBox1.Location = new System.Drawing.Point(10, 20);
			this.TextBox1.Name = "TextBox1";
			this.TextBox1.Size = new System.Drawing.Size(100, 20);
			this.TextBox1.TabIndex = 0;
			this.TextBox1.TextChanged += new System.EventHandler(this.TextBox1_TextChanged);
			// 
			// Button2
			// 
			this.Button2.Location = new System.Drawing.Point(197, 17);
			this.Button2.Name = "Button2";
			this.Button2.Size = new System.Drawing.Size(75, 25);
			this.Button2.TabIndex = 0;
			this.Button2.TabStop = false;
			this.Button2.Text = "Wyłącz.";
			this.Button2.UseVisualStyleBackColor = true;
			this.Button2.Click += new System.EventHandler(this.Button2_Click);
			// 
			// Label1
			// 
			this.Label1.AutoSize = true;
			this.Label1.Location = new System.Drawing.Point(16, 5);
			this.Label1.Name = "Label1";
			this.Label1.Size = new System.Drawing.Size(86, 13);
			this.Label1.TabIndex = 0;
			this.Label1.Text = "Podaj tutaj tekst:";
			this.Label1.Click += new System.EventHandler(this.Label1_Click);
			// 
			// Timer1
			// 
			this.Timer1.Tick += new System.EventHandler(this.Timer1_Tick);
			// 
			// Button1
			// 
			this.Button1.Location = new System.Drawing.Point(116, 17);
			this.Button1.Name = "Button1";
			this.Button1.Size = new System.Drawing.Size(75, 25);
			this.Button1.TabIndex = 0;
			this.Button1.TabStop = false;
			this.Button1.Text = "Włącz.";
			this.Button1.UseVisualStyleBackColor = true;
			this.Button1.Click += new System.EventHandler(this.Button1_Click);
			// 
			// SpamBot
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(284, 46);
			this.Controls.Add(this.Label1);
			this.Controls.Add(this.Button2);
			this.Controls.Add(this.Button1);
			this.Controls.Add(this.TextBox1);
			this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
			this.MaximizeBox = false;
			this.Name = "SpamBot";
			this.ShowIcon = false;
			this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
			this.Text = "SpamBot";
			this.Load += new System.EventHandler(this.SpamBot_Load);
			this.ResumeLayout(false);
			this.PerformLayout();

		}

		#endregion

		private System.Windows.Forms.TextBox TextBox1;
		private System.Windows.Forms.Button Button2;
		private System.Windows.Forms.Label Label1;
		private System.Windows.Forms.Timer Timer1;
		private System.Windows.Forms.Button Button1;
	}
}
