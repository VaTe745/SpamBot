﻿using System;
using System.Windows.Forms;

namespace SpamBot
{
    static class SpaamBoot
    {
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new SpamBot());
        }
    }
}
