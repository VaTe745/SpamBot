﻿using System.Windows.Forms;

namespace SpamBot
{
	public partial class SpamBot : Form
	{
		public SpamBot()
		{
			InitializeComponent();
		}

		private void SpamBot_Load(object sender, System.EventArgs e)
		{
			Button1.Enabled = false;
			Button2.Enabled = false;
		}

		private void TextBox1_TextChanged(object sender, System.EventArgs e)
		{
			if (TextBox1.TextLength < 1)
			{
				Button1.Enabled = false;
				Button2.Enabled = false;
			}
			else
			{
				Button1.Enabled = true;
				Button2.Enabled = true;
			}
		}

		private void Button1_Click(object sender, System.EventArgs e)
		{
			Timer1.Start();
		}

		private void Button2_Click(object sender, System.EventArgs e)
		{
			Timer1.Stop();
		}

		private void Label1_Click(object sender, System.EventArgs e)
		{
		}

		private void Timer1_Tick(object sender, System.EventArgs e)
		{
			SendKeys.Send(TextBox1.Text);
			SendKeys.Send("{ENTER}");
		}
	}
}
